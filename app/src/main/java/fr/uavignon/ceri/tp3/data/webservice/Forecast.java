package fr.uavignon.ceri.tp3.data.webservice;

public class Forecast {
    public final String name;
    public final int temperature;
    public final String temperatureUnit;
    public final String icon;

    public Forecast(String name, int temperature,
                    String temperatureUnit, String icon) {
        this.name = name;
        this.temperature = temperature;
        this.temperatureUnit = temperatureUnit;
        this.icon = icon;
    }
}
