package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {
    public final boolean isLoading;
    public final List<Forecast> forecasts;
    public final Throwable error;

    WeatherResult(boolean isLoading, List<Forecast> forecasts, Throwable error) {
        this.isLoading = isLoading;
        this.forecasts = forecasts;
        this.error = error;
    }

    static void transferInfo(WeatherResponse weatherInfo, City cityInfo)
    {
        cityInfo.setWindSpeed(Math.round(weatherInfo.wind.speed));
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setTemperature(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
    }

}
