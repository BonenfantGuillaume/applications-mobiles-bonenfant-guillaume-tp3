package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OWMInterface {

    @Headers("Accept: application/json; charset=utf-8")
    @GET("weather")
    Call<WeatherResponse> getForecast(@Query("q") String query,
                                      @Query("APIkey") String apiKey);
}
